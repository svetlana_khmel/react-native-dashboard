import {observable} from 'mobx';
import dataMixin from "../utils/data";
import _ from "lodash";

let index = 0;

class Store {
    @observable list={
        config: {
            username: {
               component: 'input',
               name: 'username',
               type: 'text',
               pattern: [/^[a-zA-Z0-9]+([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*[a-zA-Z0-9]+$/],
               messages: {
                   required: "This field can\'t be empty",
                   possitive: "",
                   negative: 'Usernames can consist of lowercase and capitals. Usernames can consist of alphanumeric characters. Usernames can consist of underscore and hyphens and spaces. Cannot be two underscores, two hypens or two spaces in a row. Cannot have a underscore, hypen or space at the start or end.'
               },
               min: 4,
               max: 40,
               required: true,
               label: 'User name'
           },
           password: {
                   component: 'input',
                   name: 'password',
                   type: 'password',
                   pattern: [/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/],
                   messages: {
                       required: "This field can\'t be empty",
                       possitive: "",
                       negative: "Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet and 1 Number",
                   },
                   min: 4,
                   max: 40,
                   required: true,
                   label: 'Password'
           }
        },

        url: {
            API_REPORT_URL : 'https://vast-meadow-96220.herokuapp.com/api/report',
            API_ASSORTMENT_URL : 'https://vast-meadow-96220.herokuapp.com/api/assortment'
        }
    };

    addData (data, payload) {
        this.list[data] = payload
    };

    goFetch (url, type) {
                let store = this;
                let created;
                let today;

                return fetch(url)
                      .then((response) => response.json())
                      .then((responseJson) => {
                        console.log("responseJson..... ", responseJson)
                        //return responseJson.username;
                               if (type === 'API_REPORT_URL') {
                                    //Report for all time
                                    //store.addData('userDataReport', responseJson);
                                    // Report for today only
                                    let data = dataMixin.filterData(responseJson);
                                    store.addData('userDataReport', data);
                               }

                               if (type === 'API_ASSORTMENT_URL') {
                                    store.addData('userDataAssortment', responseJson);
                               }
                      })
                      .catch((error) => {
                        console.error(error);
                      });
            }
}

const store = window.store =  new Store();
export default store;