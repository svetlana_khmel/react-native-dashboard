import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  TouchableWithoutFeedback,
  TouchableHighlight,
  View,
  PixelRatio,
  RefreshControl
} from 'react-native';

import { observer, inject} from 'mobx-react/native';

import AreaSpline from './charts/AreaSpline';
import Pie from './charts/Pie';
import Theme from './themes';
import data from '../resources/data';
import dataMixin from '../utils/data';

type State = {
  activeIndex: number,
  spendingsPerYear: any
}

@inject('store') @observer

export default class App extends Component {

  state: State;

  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      spendingsPerYear: data.spendingsPerYear,
      data: '',
      isRefreshing: false
    };

    this._onPieItemSelected = this._onPieItemSelected.bind(this);
    this._shuffle = this._shuffle.bind(this);
    this.getData - this.getData.bind(this);
    this.navigate = this.navigate.bind(this);
    this.loadData = this.loadData.bind(this);
  }

  componentWillMount () {
    this.getData();
  }

  navigate(name) {
      this.props.navigator.push({name});
  }

  getData () {
      const assortment =  this.props.store.list.userDataAssortment;
      const report =  this.props.store.list.userDataReport;

      this.setState({
          data: dataMixin.getData(assortment, report)
      });

      this.data = dataMixin.getData(assortment, report);
      console.log("______dataMixin.getData()   ", this.data);

       this.setState({
          isRefreshing: false
       });

       this.forceUpdate()
   }

   async loadData () {
       await this.props.store.goFetch(this.props.store.list.url.API_REPORT_URL, 'API_REPORT_URL');
       await this.props.store.goFetch(this.props.store.list.url.API_ASSORTMENT_URL, 'API_ASSORTMENT_URL');

       this.getData();
   }

   _onRefresh = () => {
       this.setState({isRefreshing: true});
       this.loadData();
   };

  _onPieItemSelected(newIndex){
    console.log("  ...._onPieItemSelected  ");
    this.setState({...this.state, activeIndex: newIndex, spendingsPerYear: this._shuffle(data.spendingsPerYear)});
  }

  _shuffle(a) {
      for (let i = a.length; i; i--) {
          let j = Math.floor(Math.random() * i);
          [a[i - 1], a[j]] = [a[j], a[i - 1]];
      }
      return a;
  }

  render() {
    const height = 200;
    const width = 500;

    console.log(".....dataMixin.getData()   ", this.state.data);

    return (
      <ScrollView  refreshControl={
                                   <RefreshControl
                                     refreshing={this.state.isRefreshing}
                                     onRefresh={this._onRefresh}
                                     tintColor="#ff0000"
                                     title="Loading..."
                                     titleColor="#00ff00"
                                     colors={['#ff0000', '#00ff00', '#0000ff']}
                                     progressBackgroundColor="#ffff00"
                                   />
                                 }>
        <View style={styles.topContainer}>
         <TouchableHighlight onPress={() => this.navigate('homepage')} style={styles.regularButton}>
              <Text style={styles.textRegularButton}>Back</Text>
         </TouchableHighlight>
        </View>

        <View style={styles.container} >
          <Text style={styles.chart_title}>Sells today:</Text>
          <Pie
            pieWidth={150}
            pieHeight={150}
            onItemSelected={this._onPieItemSelected}
            colors={Theme.colors}
            width={width}
            height={height}
            data={this.state.data} />
          <Text style={styles.chart_title}>Sells per month in {this.state.data[this.state.activeIndex].label}</Text>
          <AreaSpline
            width={width}
            height={height}
            data={this.state.spendingsPerYear}
            color={Theme.colors[this.state.activeIndex]} />
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  container: {
    backgroundColor:'whitesmoke',
    marginTop: 21,
  },
  chart_title : {
    paddingTop: 15,
    textAlign: 'center',
    paddingBottom: 15,
    paddingLeft: 5,
    fontSize: 24,
    backgroundColor:'#ffffff',
    color: '#995aba',
    fontWeight:'bold'
  },
  regularButton: {
      borderWidth: 1/PixelRatio.get(),
      borderColor: "#dadada",
      borderRadius: 10,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      width: 100
  },
  textRegularButton: {
     fontSize: 18,
     color: "#666",
     margin: 10
  },
}