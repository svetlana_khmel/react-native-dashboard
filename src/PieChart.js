import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PixelRatio,
  TouchableHighlight,
  ScrollView,
  RefreshControl
} from 'react-native';

import { observer, inject } from 'mobx-react/native';
import _ from 'lodash';

import Pie from './Pie';
import Lable from './Lable';

import dataMixin from '../utils/data';

@inject('store') @observer

export default class PieChart extends Component {

  constructor(props) {
    super(props);
    this.state = {
        today: '',
        data:'',
        isRefreshing: false
    }
    this.data = this.state.data;

    this.navigate = this.navigate.bind(this);
    this.loadData = this.loadData.bind(this);
  }

  componentWillMount () {
     this.getData();
  }

  navigate(name) {
    this.props.navigator.push({name});
  }

  getData () {
    const assortment =  this.props.store.list.userDataAssortment;
    const report =  this.props.store.list.userDataReport;

    this.setState({
        data: dataMixin.getData(assortment, report)
    });

    this.data = dataMixin.getData(assortment, report);

    this.setState({
        isRefreshing: false
    });

    this.forceUpdate();
  }

  async loadData () {
      await this.props.store.goFetch(this.props.store.list.url.API_REPORT_URL, 'API_REPORT_URL');
      await this.props.store.goFetch(this.props.store.list.url.API_ASSORTMENT_URL, 'API_ASSORTMENT_URL');

      this.getData();
  }

  _onRefresh = () => {
      this.setState({isRefreshing: true});
      this.loadData();
  };

  render() {
    console.log("*****  ", this.props.store);
    return (
      <View>
        <View style={styles.topContainer}>
           <TouchableHighlight onPress={() => this.navigate('homepage')} style={styles.regularButton}>
                <Text style={styles.textRegularButton}>Back</Text>
           </TouchableHighlight>
        </View>

       <ScrollView  refreshControl={
                        <RefreshControl
                          refreshing={this.state.isRefreshing}
                          onRefresh={this._onRefresh}
                          tintColor="#ff0000"
                          title="Loading..."
                          titleColor="#00ff00"
                          colors={['#ff0000', '#00ff00', '#0000ff']}
                          progressBackgroundColor="#ffff00"
                        />
                      }>
           <View style={styles.containerInfo}>
               <Text style={styles.h1}>User: {this.props.store.list.activeUser} </Text>
               <Text style={styles.h2}>Today: {this.state.today}</Text>
           </View>
           <View style={styles.group}>
              <Text style={styles.h2}>Day sells</Text>
              <Pie data={this.data}
                    outerRadius={100}
                    innerRadius={20}
                 valueMap={(datum) => datum.value} />
          </View>
          <View style={{margin: 30}}>
            <Lable data={this.data} />
          </View>
        </ ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },

     regularButton: {
         borderWidth: 1/PixelRatio.get(),
         borderColor: "#dadada",
         borderRadius: 10,
         backgroundColor: '#fff',
         alignItems: 'center',
         justifyContent: 'center',
         width: 100
     },

     containerInfo: {
         flex: 1,
         flexDirection: 'column',
         justifyContent: 'flex-start',
         alignItems: 'center',
         margin: 10
     },

     textRegularButton: {
        fontSize: 18,
        color: "#666",
        margin: 10
     },

    header: {
       fontSize: 24,
       color: '#0cc180',
       margin: 10
    },

    h1: {
       fontSize: 30,
       color: '#995aba'
    },

    h2: {
       fontSize: 24,
       color: '#995aba',
       margin: 10
    },

    h3: {
       fontSize: 20,
       color: '#efbd0e'
    },

    h4: {
       fontSize: 15,
       color: '#efbd0e'
    },
    group: {
        flex: 1,
        marginBottom: 15,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
    }
});