import React, {
    Component
} from 'react';

import {
    View,
    StyleSheet,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    Text,
    ScrollView,
    Dimensions,
    RefreshControl
} from 'react-native';

import { observer, inject} from 'mobx-react/native';
import dataMixin from '../utils/data';
import _ from 'lodash';

let {height, width} = Dimensions.get('window')

@inject('store') @observer

export default class Charts extends Component {
    constructor(props){

    super(props);
        this.state = {
            today: '',
            data:'',
            isRefreshing: false
        }
        this.data = this.state.data;

        this.navigate = this.navigate.bind(this);
        this.graphRender = this.graphRender.bind(this);
        this.getPercent = this.getPercent.bind(this);
        this.getBiggestValue = this.getBiggestValue.bind(this);
        this.loadSockets = this.loadSockets.bind(this);
        this.loadData = this.loadData.bind(this);
    }

    componentWillMount () {
         this.getData();

         this.loadSockets ();
    }

    navigate(name) {
        this.props.navigator.push({name});
    }

    loadSockets () {
        var ws = new WebSocket('ws://localhost:3000');
         console.log('loadSockets');



        ws.onopen = () => {
          // connection opened
           console.log('ws unopen', e.data);

           ws.on('news', function (data) {
            console.log(data);
            socket.emit('my other event', { my: 'data' });
        });

          ws.send('something'); // send a message
        };

        ws.onmessage = (e) => {
          // a message was received
          console.log(e.data);
        };

        ws.onerror = (e) => {
          // an error occurred
          console.log(e.message);
        };

        ws.onclose = (e) => {
          // connection closed
          console.log(e.code, e.reason);
        };
    }

    getData () {
        const assortment =  this.props.store.list.userDataAssortment;
        const report =  this.props.store.list.userDataReport;

        this.setState({
            data: dataMixin.getData(assortment, report)
        });

        this.data = dataMixin.getData(assortment, report)

        this.setState({
            isRefreshing: false
        });

        this.forceUpdate();
     }

     getBiggestValue (type) {
        if(type === 'value') {
            let arr = _.orderBy(this.state.data, ['value'], ['desc']);
            return arr[0].value;
        }

        if(type === 'quantity') {
            let arr = _.orderBy(this.state.data, ['quantity'], ['desc']);
            return arr[0].quantity;
        }
     }

    getPercent(type, num) {
       if(type === 'value') {
            return num*100/this.getBiggestValue('value');
       }

       if(type === 'quantity') {
            return num*100/this.getBiggestValue('quantity');
       }
    }

    graphRender() {
        return this.state.data.map((data, index)=> {
                    return(
                        <View style={styles.group} key={index}>
                            <Text style={styles.h3}>{data.label}</Text>
                            <Text style={styles.h4}>Summ: {data.value} Quantity: {data.quantity}</Text>
                            <TouchableOpacity style={{width: this.getPercent('value', data.value) + '%', height: 20, backgroundColor: '#995aba', borderRadius: 1}}></TouchableOpacity>
                            <TouchableOpacity style={{width: this.getPercent('quantity', data.quantity) + '%', height: 20, backgroundColor: '#efbd0e', borderRadius: 1}}></TouchableOpacity>
                        </View>
                    );
               })
    }

    async loadData () {
        await this.props.store.goFetch(this.props.store.list.url.API_REPORT_URL, 'API_REPORT_URL');
        await this.props.store.goFetch(this.props.store.list.url.API_ASSORTMENT_URL, 'API_ASSORTMENT_URL');

        this.getData();
    }

    _onRefresh = () => {
        this.setState({isRefreshing: true});
        this.loadData();
    };

    render() {
         this.loadSockets ();

        return (
         <View>
          <View style={styles.topContainer}>
             <TouchableHighlight onPress={() => this.navigate('homepage')} style={styles.regularButton}>
                  <Text style={styles.textRegularButton}>Back</Text>
             </TouchableHighlight>
          </View>

           <ScrollView refreshControl={
                                 <RefreshControl
                                   refreshing={this.state.isRefreshing}
                                   onRefresh={this._onRefresh}
                                   tintColor="#ff0000"
                                   title="Loading..."
                                   titleColor="#00ff00"
                                   colors={['#ff0000', '#00ff00', '#0000ff']}
                                   progressBackgroundColor="#ffff00"
                                 />
                               }>
            <View style={styles.containerInfo}>
                <Text style={styles.h1}>User: {this.props.store.list.activeUser} </Text>
                <Text style={styles.h2}>Today: {this.state.today}</Text>
            </View>
            <View style={styles.graphcontainer}>
                {this.graphRender()}
            </View>
           </ ScrollView>
         </View>
        );
    }
}

const styles = StyleSheet.create({
    containerInfo: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        margin: 10
    },
    graphcontainer: {
        marginLeft: 30,
        marginRight: 30
    },
    regularButton: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#dadada",
        borderRadius: 10,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        width: 100
    },
    textRegularButton: {
       fontSize: 18,
       color: "#666",
       margin: 10
    },
    header: {
       fontSize: 24,
       color: '#995aba',
       margin: 10
    },
    h1: {
       fontSize: 30,
       color: '#995aba'
    },
    h3: {
       fontSize: 20,
       color: '#995aba'
    },
    h4: {
       fontSize: 15,
       color: '#efbd0e'
    },
    group: {
        flex: 1,
        marginBottom: 15
    }
});