import React, { Component, PropTypes } from 'react';
import createFragment from 'react-addons-create-fragment';

import {
  ART,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import * as d3 from 'd3';

const {
  Group,
  Surface
} = ART;

import Arc from './Arc';

export default class Pie extends Component {

  constructor(props) {
    super(props);
    this.colors = d3.schemeCategory20c;
    this.arcs = d3
                .pie()
                .value(this.props.valueMap)
                .padAngle(this.props.padAngle)
                (this.props.data);
    this.renderLables = this.renderLables.bind(this);
  };

  renderLables () {
    return this.arcs.map((arcData,index) => {

        return (
            <View style={styles.row}  key={index}>
                <TouchableOpacity style={{width:30, height: 30, borderRadius: 50, backgroundColor: this.colors[index]}}></TouchableOpacity>
                <Text style={{fontSize: 20, color: '#995aba', marginLeft: 15}}>{arcData.data.label} {arcData.data.value}</Text>
            </View>
        )
    });
  }

  render() {
    return (
      <View>
             { this.renderLables() }
      </View>
    );
  }
}

const styles = StyleSheet.create({
    row: {
        flex: 1,
        flexDirection: 'row'
    }
});

