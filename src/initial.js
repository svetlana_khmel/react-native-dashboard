import React, { Component } from 'react';
import { View, Text, Navigator, TouchableHighlight, PixelRatio, StyleSheet } from 'react-native';
import PieChart from "./PieChart";
import Column from "./ColumnChart";
import Button from "./components/NavButton";


export default class Initial extends Component {

   constructor () {
        super();
   }

  render() {
        return (
           <View style={styles.container}>
                <Text style={styles.header}>Day sells:</Text>
                <Button navigator={this.props.navigator} navigate={'pie'} val={'Pie'} styleType={1} />
                <Button navigator={this.props.navigator} navigate={'animatedPie'} val={'Active Pie'} styleType={2} />
                <Button navigator={this.props.navigator} navigate={'column'} val={'Bars'} styleType={3} />
           </View>
        );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    regularButtonOne: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#26a69a",
        borderRadius: 10,
        backgroundColor: '#26a69a',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    },
    regularButtonTwo: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#009688",
        borderRadius: 10,
        backgroundColor: '#009688',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    },
    regularButtonThree: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#00897b",
        borderRadius: 10,
        backgroundColor: '#00897b',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    },
    textRegularButton: {
       fontSize: 24,
       color: "#fff",
       margin: 10
    },
    header: {
       fontSize: 35,
       color: '#995aba',
       margin: 10
    }
});