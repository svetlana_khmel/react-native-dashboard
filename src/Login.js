import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    StyleSheet,
    PixelRatio
} from 'react-native';

import axios from 'axios';
import sha256 from 'sha256';
import {observer, inject} from 'mobx-react/native';

const API_URL = 'https://vast-meadow-96220.herokuapp.com/api/users/login';
const API_USERS_URL = 'https://vast-meadow-96220.herokuapp.com/api/users';
const API_REPORT_URL = 'https://vast-meadow-96220.herokuapp.com/api/report';
const API_ASSORTMENT_URL = 'https://vast-meadow-96220.herokuapp.com/api/assortment';

@inject('store') @observer

export default class Login extends Component {
    constructor () {
        super();

        this.state = {
            textInputLoginPlaceholder: '',
            textInputPasswordPlaceholder: '',
            loginValidation: '',
            loginError: '',
            passwordValidation: '',
            passwordError: '',
            loginValue: '',
            passwordValue:'',
            loginStatus: '',
            passwordStatus: '',
            userRequiredMessage: '',
            userNegativeMessage: '',
            passwordRequiredMessage: '',
            passwordNegativeMessage: '',
            activeUser:''
        }

        this.navigate = this.navigate.bind(this);
        this.validate = this.validate.bind(this);
        this.fetchData = this.fetchData.bind(this);
        //this.goFetch = this.goFetch.bind(this);
        this.getUserData = this.getUserData.bind(this);
        this.goMain = this.goMain.bind(this);


        this.loginStatus="";
        this.passwordStatus="";
    }

    navigate (name) {
        this.props.navigator.push({
            name:name
        });
     }

    validate () {
            const fields = ['username', 'password'];
            const store = this.props.store;
            const state = this.state;

            fields.map(function (i) {
                console.log('fields . . .', i, fields, '   ', fields[i], ' . .. ', store);

                let regexp = store.list.config[i].pattern[0];
                let userRequired = store.list.config[i].required;
                let requiredMessage = store.list.config[i].messages.required;
                let negativeMessage = store.list.config[i].messages.negative;

                if (i == 'username') {

                    if (!regexp.test(state.loginValue) || state.loginValue === '') {
                         this.setState({
                             loginStatus: 'negative',
                             loginNegativeMessage: store.list.config['username'].messages.negative
                         });
                    } else {
                        this.setState({
                             loginStatus: 'positive'
                         });
                    }
                }

                if (i == 'password') {

                    if (!regexp.test(state.passwordValue) || state.passwordValue === '') {
                         this.setState({
                              passwordStatus: 'negative',
                              passwordNegativeMessage: store.list.config['password'].messages.negative
                          })
                          this.passwordStatus='negative';
                    } else {
                         this.setState({
                              passwordStatus: 'positive'
                          });
                          this.loginStatus = 'positive';
                    }

                    this.setSubmitData();
                }

            }.bind(this));
        }

        setSubmitData (event, status) {
                let password='';

                if (this.loginStatus === 'positive' && this.passwordStatus === 'positive') {
                    password = sha256(this.state.passwordValue);
                }

                this.setState({
                    passwordValue: password
                });

                this.fetchData();
        }

        fetchData() {
            var data = {
                  "username": this.state.loginValue,
                  "password": this.state.passwordValue
            }

            // Convert JSON to decoded string like  "username=test3&password=Test3Test"
            var params = Object.keys(data).map(function(k) {
                return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
            }).join('&');

            console.log("__params  ", params);

            var xhr = new XMLHttpRequest();
            xhr.open("POST", API_URL, true);

            //Send the proper header information along with the request
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function() {
                //Call a function when the state changes.
                if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                    console.log(xhr.responseText);
                    this.getUserData(xhr.responseText);
                    return;
                }

                if (xhr.status!= 200 ) {
                    this.setState({
                        loginStatus: 'failed',
                        loginNegativeMessage:'Login failed. Check your credentials or connection.'
                    });
                    console.log('XMLhttpRequest error', xhr.statusText, 'XMLhttpRequest ... ', xhr);
                }
            }.bind(this)

            xhr.send(params);
         }

         getUserData (data) {

            const user = JSON.parse(data).username;

            this.setState({
                activeUser: user,
                loginStatus:''
            });

            this.props.store.addData('activeUser', user);
            this.goMain()
        }

        async goMain () {
            await this.props.store.goFetch(API_REPORT_URL, 'API_REPORT_URL');
            await this.props.store.goFetch(API_ASSORTMENT_URL, 'API_ASSORTMENT_URL');
            this.navigate('homepage');
        }

        render () {
            const { image, store } = this.props;

                return (
                    <View style={styles.container}>
                        <Text style={styles.login}>Login</Text>
                        <TextInput style={styles.input}
                            value={this.state.loginValue}
                            defaultValue='Login'
                            onChangeText={(loginValue) => this.setState({loginValue})}
                        />

                        {this.state.loginStatus==='negative' &&
                              <Text style={styles.validation}>{this.state.loginNegativeMessage}</Text>
                        }

                        {this.state.loginStatus==='failed' &&
                              <Text style={styles.error}>{this.state.loginNegativeMessage}</Text>
                        }

                        <Text style={styles.login}>Password</Text>

                        <TextInput style={styles.input}
                                value={this.state.passwordValue}
                                onChangeText={(passwordValue) => this.setState({passwordValue})}
                        />

                         {this.state.passwordStatus==='negative' &&
                              <Text style={styles.validation}>{this.state.passwordNegativeMessage}</Text>
                         }
                        <TouchableOpacity style={styles.button}  onPress={() => this.validate  ()}>
                            <Text style={styles.buttonText}>Login</Text>
                        </ TouchableOpacity>
                    </View>
                );
    };
}

const styles = StyleSheet.create({
    container: {
        margin: 50
    },
    login: {
        fontSize: 30,
        color: "#333"
    },
    validation: {
        fontSize: 12,
        color: "green"
    },
    error: {
        color: 'red'
    },
    input: {
        fontSize: 24,
        color: "#666",
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#dadada"
    },
    button: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#995aba",
        borderRadius: 10,
        backgroundColor: '#995aba',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50
    },
    buttonText: {
        fontSize: 24,
        color: "#fff",
        margin: 10
    }
});
