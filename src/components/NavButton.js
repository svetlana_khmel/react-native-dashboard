import React, { Component } from 'react';
import { View, Text, Navigator, TouchableHighlight, PixelRatio, StyleSheet } from 'react-native';

export default class Button extends Component {

    constructor () {
        super();

        this.navigate = this.navigate.bind(this);
   }

    navigate (name) {
        this.props.navigator.push({name});
    }

    render () {
        let setStyle;

        if (this.props.styleType === 1) {
            setStyle = styles.regularButtonOne;
        }

        if (this.props.styleType === 2) {
            setStyle = styles.regularButtonTwo;
        }

        if (this.props.styleType === 3) {
            setStyle = styles.regularButtonThree;
        }

        return (
            <TouchableHighlight  onPress={() => this.navigate(this.props.navigate)} style={setStyle}>
               <Text style={styles.textRegularButton}>{this.props.val}</Text>
            </TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
    regularButtonOne: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#995aba",
        borderRadius: 10,
        backgroundColor: '#995aba',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    },
    regularButtonTwo: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#b56bdb",
        borderRadius: 10,
        backgroundColor: '#b56bdb',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    },
    regularButtonThree: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#cf7ef9",
        borderRadius: 10,
        backgroundColor: '#cf7ef9',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    },
    textRegularButton: {
       fontSize: 24,
       color: "#fff",
       margin: 10
    }
});