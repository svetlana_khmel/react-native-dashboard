# README #

### What is this repository for? ###

* "Dashboard" application shows particular user's day's sells . This is an mobile application for CapApp application.(https://bitbucket.org/svetlana_khmel/cap-v2)

The goal is:

 1. To create charts based on user's data using d3 Art Library.

 2. To explore charts, available for React.

 3. Use MobX as storage provider.

 4. Use react networking: fetch (axios), Web Sockets, regular XMLhttpRequest.


http://www.reactd3.org/docs/basic/


On google play:

[![en_badge_web_generic.png](https://bitbucket.org/repo/9pKMbE6/images/3446325083-en_badge_web_generic.png)](https://play.google.com/store/apps/details?id=com.dashboard_native&rdid=com.dashboard_native)


Screenshots:

![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/c_scale,w_250/v1493388725/dashboard1_qupb08.png)
![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/c_scale,w_250/v1493388726/dashboard2_ubnnnj.png)
![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/c_scale,w_250/v1493388726/dashboard3_secnts.png)

--

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

* Configuration

Brew install node
Brew install watchman
Npm install -g react-native-cil
react-native init mafia

react-native run-ios
react-native run-android

IS BETTER TO START WITH:
 react-native start

https://developer.android.com/studio/index.html?utm_source=android-studio

Install environment:
https://facebook.github.io/react-native/docs/getting-started.html



Release:
http://facebook.github.io/react-native/docs/signed-apk-android.html

Generating the release APK
Simply run the following in a terminal:

$ cd android && ./gradlew assembleRelease

Generate key
 keytool -genkey -v -keystore mafia.keystore -alias mafia_main -keyalg RSA -keysize 2048 -validity 10000


* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## Install Java ##

Don't rely on Oracle to install Java properly on your Mac.

Use Homebrew:

brew update
brew cask install java
If you want to manage multiple versions of Java on your Mac, consider using jenv.


java -version


Еo run a different version of Java, either specify the full path, or use the java_home tool:

% /usr/libexec/java_home -v 1.8.0_06 --exec javac -version

For example, to uninstall 8u6:

% rm -rf jdk1.8.0_06.jdk

##  Use REACT_DEBUGGER env of react-native packager (react-native-debugger-open)

REACT_DEBUGGER="rndebugger-open --open --port 8081" npm start


*** Debugger by default:

http://localhost:8081/debugger-ui

##Charts: The goal was to study hoe to work with D3 Art and react-native-svg, but it is real hell and then I have desided to use this library:


https://www.npmjs.com/package/react-native-pathjs-charts
https://github.com/capitalone/react-native-pathjs-charts
https://github.com/capitalone/react-native-pathjs-charts/wiki/Pie-Charts


Helpful article: Animated Charts in React Native using D3 and ART
https://medium.com/the-react-native-log/animated-charts-in-react-native-using-d3-and-art-21cd9ccf6c58


** How to add icons to React Native app



for IOS:
you should set AppIcon in Images.xcassets.
you should upload 9 different size icons 29pt 29pt*2 29pt*3 40pt*2 40pt*3 57pt 57pt*2 60pt*2 60pt*3.

picture for setting Images.xcassets
for Android:
put ic_launcher.png to folder [PrjDir]/android/app/src/main/res/mipmap-*.
72*72 ic_launcher.png to mipmap-hdpi.
48*48 ic_launcher.png to mipmap-mdpi.
96*96 ic_launcher.png to mipmap-xhdpi.
144*144 ic_launcher.png to mipmap-xxhdpi.

** PUBLISH

Generating the release APK Simply run the following in a terminal:
           $ cd android && ./gradlew assembleRelease


** Go to android/app in your project folder and run
   keytool -genkey -v -keystore my-release-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000

   Example:

   Generate key keytool -genkey -v -keystore mafia.keystore -alias mafia_main -keyalg RSA -keysize 2048 -validity 10000
