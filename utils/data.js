import _ from 'lodash';

var data = {

     getData (assortment, report) {

        let assortmentItem = [];
        let chartData = [];

        _.each(assortment, function(value, key) {
             assortmentItem.push({'label':value.name, 'value': 0, 'quantity': 0});
        });

        _.each(assortmentItem, function(obj, key) {

            _.each(report, function(rep, repkey) {

                 if(rep.name === obj.label) {
                    obj.value += rep.price;
                    obj.quantity +=1;
                    console.log("++",obj.quantity);
                 }

            });
        });

        return assortmentItem;
      },

      getDate (time) {
         let date = time? new Date(time):new Date();
         let day = date.getDate();
         let month = date.getMonth() + 1;
         let year = date.getFullYear();

         if (day < 10 ) {
             day = '0' + day;
         }

         if (month < 10 ) {
             month = '0' + month;
         }

         let data = day + "/" + month + "/" + year;

         return data;
        },

        filterData (responseJson) {
            let self = this;
            let dataArr=[];

                _.forEach(responseJson, function (value, index) {
                    created = self.getDate(parseInt(value.created_at));
                    today = self.getDate();

                    if (created && created === today) {
                         dataArr.push(value);
                    }
                });

                return dataArr;
        }
}

module.exports = data;