import React, {
    Component
} from 'react';

import {
    AppRegistry,
    Text,
    View,
    ScrollView,
    Navigator
} from 'react-native';

import PieChart from './src/PieChart';
import ColumnChart from './src/ColumnChart.js';
import AnimatedCharts from './src/AnimatedCharts.js';

import Initial from './src/initial';
import Login from './src/Login';
import Store from './mobx/listStore';

import { Provider as MobXProvider, observer } from 'mobx-react/native';

@observer

export default class Routing extends Component {
    constructor () {
        super ()

        this.renderScene = this.renderScene.bind(this);
    }

    renderScene(route, navigator) {
         if(route.name === 'homepage') {
            return <Initial navigator={navigator} />
         }

         if (route.name === 'login') {
            return <Login navigator={navigator} />
         }

         if (route.name === 'statistic') {
            return <Statistic navigator={navigator} />
         }

         if (route.name === 'pie') {
            return <PieChart navigator={navigator} />
         }

         if (route.name === 'animatedPie') {
            return <AnimatedCharts navigator={navigator} />
         }

         if (route.name === 'column') {
            return <ColumnChart navigator={navigator} />
         }
    }

    render () {
        console.log("***  index ", Store);
        return (
        <MobXProvider store={Store}>
            <View style={{flex: 1}}>
                <Navigator
                    initialRoute={
                        {
                            name: 'login',
                            store: Store,
                            passProps: {
                                store: Store
                            }
                        }
                    }

                    renderScene={this.renderScene}
                    configureScene={(route, routeStack) =>
                        Navigator.SceneConfigs.FloatFromRight}

                    />
            </View>
         </ MobXProvider>
        );
    }
}

AppRegistry.registerComponent('dashboard_native', () => Routing);